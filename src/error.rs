use std::error::Error as StdError;
use std::fmt::{self, Display};

use crate::expr::Expr;
use crate::stack::Stack;
use crate::token::Token;

pub type Result<'a, T> = std::result::Result<T, Error<'a>>;

#[derive(Clone, Hash, Debug, PartialEq, Eq)]
pub enum Error<'a> {
    UnknownIdent(&'a str),
    UnmatchedLParen,
    UnmatchedRParen,
    UnexpectedToken(Token<'a>),
    UnexpectedEnd,
    NoArgument(usize, Stack<Expr>),
}

impl<'a> Display for Error<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::UnknownIdent(ident) => write!(f, "Unknown identifier {}", ident),
            Self::UnmatchedLParen => write!(f, "Unmatched '('"),
            Self::UnmatchedRParen => write!(f, "Unmatched ')'"),
            Self::UnexpectedToken(tok) => write!(f, "Unexpected token {}", tok),
            Self::UnexpectedEnd => write!(f, "Unexpected end of token stream"),
            Self::NoArgument(i, stack) => write!(f, "No argument {} for stack {:?}", i, stack),
        }
    }
}

impl<'a> StdError for Error<'a> {}
