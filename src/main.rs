#![feature(let_else)]

use std::env;
use std::error::Error;
use std::fs;

use lambda::parse;

#[allow(unused_variables)]
fn main() -> Result<(), Box<dyn Error>> {
    let mut args = env::args();
    let name = args.next().unwrap();
    let Some(progfile) = args.next() else {
        eprintln!("USAGE: {} <program>", name);
        return Err("argument expected".into());
    };
    let prog = fs::read_to_string(progfile)?;

    let expr = parse(&prog).map_err(|x| x.to_string())?;
    let res = expr.eval();
    println!("{}", res);
    Ok(())
}
