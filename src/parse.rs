use crate::Error as ParseError;
use crate::Expr;
use crate::Result as ParseResult;
use crate::Stack;
use crate::Token;

pub fn parse(s: &str) -> ParseResult<'_, Expr> {
    let toks = Token::tokenize(s);
    let mut stack = Stack::new();
    _parse(&toks, &mut stack)
}

fn _parse_prefix<'a>(
    toks: &[Token<'a>],
    stack: &mut Stack<&'a str>,
) -> ParseResult<'a, (Expr, usize)> {
    match toks {
        [Token::NewLine, ..] => _parse_prefix(&toks[1..], stack).map(|(expr, i)| (expr, i + 1)),

        [Token::Lambda, Token::Ident(ident), Token::Dot, ..] => {
            stack.push(ident);
            let ret = _parse_prefix(&toks[3..], stack)
                .map(|(term, i)| (Expr::Lambda(Box::new(term)), i + 3));
            stack.pop();
            ret
        },

        [Token::Ident(ident), Token::Equals, ..] => {
            if let Some(newline) = toks.iter().position(|tok| *tok == Token::NewLine) {
                let arg = _parse(&toks[2..newline], stack)?;
                stack.push(ident);
                let term = Expr::lambda(_parse(&toks[newline..], stack)?);
                Ok((term.apply(arg), toks.len()))
            } else {
                Err(ParseError::UnexpectedEnd)
            }
        },

        [Token::Ident(ident), ..] => match stack.index(ident) {
            Some(i) => Ok((Expr::Ident(i), 1)),
            None => Err(ParseError::UnknownIdent(ident)),
        },

        [Token::LParen, ..] => {
            let mut nested_depth = 0;
            let mut j = 0;
            for (i, &tok) in toks.iter().enumerate() {
                if tok == Token::LParen {
                    nested_depth += 1;
                } else if tok == Token::RParen {
                    nested_depth -= 1;
                }
                if nested_depth == 0 {
                    j = i;
                    break;
                }
            }
            if j == 0 || nested_depth != 0 {
                Err(ParseError::UnmatchedLParen)
            } else {
                _parse(&toks[1..j], stack).map(|st| (st, j + 1))
            }
        },

        [Token::RParen, ..] => Err(ParseError::UnmatchedRParen),
        [tok, ..] => Err(ParseError::UnexpectedToken(*tok)),
        [..] => Err(ParseError::UnexpectedEnd),
    }
}

fn _parse<'a>(toks: &[Token<'a>], stack: &mut Stack<&'a str>) -> ParseResult<'a, Expr> {
    let (mut expr, mut i) = _parse_prefix(toks, stack)?;
    while i < toks.len() {
        let (next, j) = _parse_prefix(&toks[i..], stack)?;
        i += j;
        expr = expr.apply(next);
    }
    Ok(expr)
}
