use std::fmt::{self, Debug, Display};

#[derive(Clone, Hash, PartialEq, Eq)]
pub struct Stack<T: Eq>(Vec<T>);

impl<T: Eq> Stack<T> {
    pub fn new() -> Self {
        Self(Vec::new())
    }

    pub fn index(&self, t: &T) -> Option<usize> {
        self.iter().position(|other| other == t)
    }

    pub fn iter(&self) -> impl Iterator<Item = &T> {
        self.0.iter().rev()
    }

    pub fn iter_mut(&mut self) -> impl Iterator<Item = &mut T> {
        self.0.iter_mut().rev()
    }

    pub fn get(&self, i: usize) -> Option<&T> {
        self.0.get(self.len().checked_sub(i + 1)?)
    }

    pub fn push(&mut self, t: T) {
        self.0.push(t)
    }

    pub fn pop(&mut self) -> Option<T> {
        self.0.pop()
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }
}

impl<T: Eq + Clone> Stack<&T> {
    pub fn cloned(&self) -> Stack<T> {
        Stack(self.0.iter().map(|&x| x.clone()).collect())
    }
}

impl<T: Eq> Default for Stack<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T: Eq + Debug> Debug for Stack<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[")?;
        self.iter()
            .map(Some)
            .intersperse(None)
            .try_for_each(|x| match x {
                Some(x) => write!(f, "{:?}", x),
                None => write!(f, " "),
            })?;
        write!(f, "]")?;
        Ok(())
    }
}

impl<T: Eq + Display> Display for Stack<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[")?;
        self.iter()
            .map(Some)
            .intersperse(None)
            .try_for_each(|x| match x {
                Some(x) => write!(f, "{}", x),
                None => write!(f, " "),
            })?;
        write!(f, "]")?;
        Ok(())
    }
}
