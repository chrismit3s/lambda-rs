use std::fmt::{self, Display};

use crate::token::LAMBDA;
use crate::Stack;

const DEBUG: bool = false;

macro_rules! indent {
    ($val:ident.$method:ident($( $arg:expr ),* $( , )?); $indent:expr, $name:expr, ($fmt:literal $( , $args:expr )*), $stack:ident $( , )?) => {{
        const INDENT: &str = "\u{2502}   ";
        if DEBUG {
            let fill = std::iter::repeat(INDENT).take($indent).collect::<String>();

            let prev = $val.clone();
            eprintln!(
                concat!("{}\u{250C}{}\u{2524} ", $fmt, " stack: [{}]"),
                fill,
                $name,
                $( $args ),*,
                $stack
                    .iter()
                    .map(ToString::to_string)
                    .intersperse_with(|| String::from(" "))
                    .collect::<String>()
            );
            let ret = $val.$method($( $arg ),*);
            eprintln!(
                "{}\u{2514}> {}{}",
                fill,
                ret,
                if ret == *prev { "" } else { "!!!" }
            );
            ret
        } else {
            $val.$method($( $arg ),*)
        }
    }};
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum Expr {
    Lambda(Box<Expr>),
    Apply { lambda: Box<Expr>, arg: Box<Expr> },
    Ident(usize),
}

impl Expr {
    pub fn church_encode(n: usize) -> Self {
        let mut expr = Self::ident(0);
        for _ in 0..n {
            expr = Self::ident(1).apply(expr);
        }
        Self::lambda(Self::lambda(expr))
    }

    pub fn identity() -> Self {
        Self::lambda(Self::ident(0))
    }

    pub fn lambda(expr: Self) -> Self {
        Self::Lambda(Box::new(expr))
    }

    pub fn ident(i: usize) -> Self {
        Self::Ident(i)
    }

    pub fn apply(self, arg: Self) -> Self {
        Self::Apply {
            lambda: Box::new(self),
            arg: Box::new(arg),
        }
    }

    pub fn is_lambda(&self) -> bool {
        matches!(self, Self::Lambda(..))
    }

    pub fn is_apply(&self) -> bool {
        matches!(self, Self::Apply { .. })
    }

    pub fn is_ident(&self) -> bool {
        matches!(self, Self::Ident(..))
    }

    pub fn ident_map<F>(&mut self, f: F)
    where
        F: Fn(usize) -> usize + Clone,
    {
        self._ident_map(0, f)
    }

    fn _ident_map<F>(&mut self, depth: usize, f: F)
    where
        F: Fn(usize) -> usize + Clone,
    {
        match self {
            Self::Lambda(lambda) => lambda._ident_map(depth + 1, f),
            Self::Apply { lambda, arg } => {
                lambda._ident_map(depth, f.clone());
                arg._ident_map(depth, f)
            },
            Self::Ident(i) if *i >= depth => *i = f(*i),
            _ => (),
        }
    }

    pub fn eval(self) -> Self {
        let depth = 0;
        let mut stack = Stack::<Self>::new();

        let s = Box::new(self);
        indent!(s._eval(&mut stack, depth + 1); depth, "inital", ("{}", *s), stack)
    }

    fn _eval(self, stack: &mut Stack<Self>, depth: usize) -> Self {
        match self {
            Self::Lambda(lambda) => {
                stack.push(Self::ident(0));
                let ret = Self::lambda(
                    indent!(lambda._eval(stack, depth + 1); depth, "def", ("{}{}", LAMBDA, &lambda), stack),
                );
                stack.pop();
                ret
            },
            Self::Apply { lambda, arg } => {
                let mut arg = indent!(
                    arg._eval(stack, depth + 1);
                    depth,
                    "arg",
                    ("{}", &arg),
                    stack,
                );
                let lambda = indent!(
                    lambda._eval(stack, depth + 1);
                    depth,
                    "lambda",
                    ("{}", &lambda),
                    stack,
                );

                if let Self::Lambda(expr) = lambda {
                    arg.ident_map(|i| i + 1);
                    stack.push(arg.clone());
                    let mut ret = indent!(expr._eval(stack, depth + 1); depth, "apply", ("{}{} {}", LAMBDA, &expr, &arg), stack);
                    stack.pop();
                    ret.ident_map(|i| i - 1);
                    ret
                } else {
                    lambda.apply(arg)
                }
            },
            Self::Ident(i) => match stack.get(i) {
                Some(expr) => {
                    let mut expr = expr.clone();
                    expr.ident_map(|j| j + i);
                    expr
                },
                None => {
                    panic!("ERROR: INDEX OUT OF BOUNDS");
                },
            },
        }
    }
}

impl Display for Expr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Lambda(lambda) =>
                if lambda.is_apply() {
                    write!(f, "{}({})", LAMBDA, lambda)
                } else {
                    write!(f, "{}{}", LAMBDA, lambda)
                },
            Self::Apply { lambda, arg } => {
                if lambda.is_lambda() {
                    write!(f, "({})", lambda)?;
                } else {
                    write!(f, "{}", lambda)?;
                }
                if arg.is_ident() {
                    write!(f, "{}", arg)?;
                } else {
                    write!(f, "({})", arg)?;
                }
                Ok(())
            },
            Self::Ident(i) =>
                if *i < 10 {
                    write!(f, "{}", i)
                } else {
                    write!(f, "[{}]", i)
                },
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::parse;

    use std::iter;

    #[rustfmt::skip]
    const BASICS: &[(&str, &str)] = &[
        (r"\a.((\b.\c.a)(\x.x))", r"\x.\y.x"),
        (r"\b.\c.(b((\x.\y.(x y)) b c))", r"\x.\y.(x(x y))"),
        (r"\b.\c.((\x.\y.(x y)) b c)", r"\x.\y.(x y)"),
        (r"\b.((\x.\y.x) b)", r"\x.\y.x"),
        (r"(\z.((\a.\b.a)(\b.z)))(\x.x)", r"\x.\y.\z.z"),
        (r"(\z.((\a.\b.a)(\x.z)))(\x.x)", r"\x.\y.\z.z"),
        (r"(\z.((\a.\b.a)(\x.(z x))))(\x.x)", r"\x.\y.y"),
        (r"(\z.((\a.\b.(b(a b)))(\q.(q(z q)))))(\x.x)", r"\x.(x(x x))"),
        (r"(\z.((\a.\b.(a b))(\q.(z q))))(\x.x)", r"\x.x"),
        (r"(\z.((\a.\b.(a b))(\q.z)))(\x.x)", r"\x.\y.y"),
        (r"\z.((\a.\b.(a b))(\q.z))", r"\x.\y.x"),
        (r"\a.((\b.\c.a)(\x.x))", r"\x.\y.x"),
        (r"(\zero.((\a.\b.\c.(b(a b c)))(\b.\c.(b(zero b c)))))(\x.\y.y)", r"\x.\y.(x (x y))"),
        (r"(\z.((\s.(s z)(\n.\f.\x.(f(n f x))))))(\f.\x.x)", r"\x.\y.(x y)"),
        (r"\b.\c.(b((\x.\y.(x y)) b c))", r"\x.\y.(x (x y))"),
        (r"\a.((\b.\c.a)(\x.x))", r"\x.\y.x"),
        (r"(\a.((\b.\c.(c (b c))) (\y.(y (a y)))))(\x.x)", r"\x.(x (x x))"),
    ];

    #[rustfmt::skip]
    const SCOPING: &[(&str, &str)] = &[
        (r"\a.\a.a", r"\a.\b.b"),
        (r"\a.\a.\a.\a.\a.\a.\a.a", r"\a.\b.\c.\d.\e.\f.\g.g"),
    ];

    const IDENTITY: &str = r"\x.x";

    const TOP: &str = r"\t.\f.t";
    const BOTTOM: &str = r"\t.\f.f";

    const ZERO: &str = r"\f.\x.x";
    const SUCC: &str = r"\n.\f.\x.(f(n f x))";

    const NAT_LIMIT: usize = 20;

    macro_rules! test_example {
        { $name:ident, $path:literal } => {
            #[test]
            fn $name() {
                const PROGRAM: &str = include_str!($path);
                let parsed = parse(PROGRAM).unwrap();
                dbg!(parsed.eval());
            }
        };
    }

    macro_rules! assert_prog {
        ($prog:expr, $expr:expr) => {{
            let prog: &str = $prog;
            let expected_expr: Expr = $expr;
            let got_expr = parse(prog).map(Expr::eval);
            assert_eq!(
                got_expr,
                Ok(expected_expr.clone()),
                "prog: {} => {:?}, expected: {}",
                prog,
                got_expr,
                expected_expr
            )
        }};
        ($prog:expr, $expr:expr, $fmt:literal $( , $arg:expr )*) => {{
            let prog: &str = $prog;
            let expected_expr: Expr = $expr;
            let got_expr = parse(prog).map(Expr::eval);
            assert_eq!(
                got_expr,
                Ok(expected_expr.clone()),
                concat!("prog: {} => {}, expected: {}, ", $fmt),
                prog,
                match &got_expr {
                    Ok(expr) => expr.to_string(),
                    Err(err) => err.to_string()
                },
                expected_expr
                $( , $arg )*
            )
        }};
    }

    #[test]
    fn test_identity() {
        let identity = Expr::lambda(Expr::ident(0));
        assert_prog!(IDENTITY, identity);
    }

    #[test]
    fn test_bool() {
        let top = Expr::lambda(Expr::lambda(Expr::ident(1)));
        assert_prog!(TOP, top);

        let bottom = Expr::lambda(Expr::lambda(Expr::ident(0)));
        assert_prog!(BOTTOM, bottom);
    }

    #[test]
    fn test_nat_simple() {
        fn create_nat(n: usize) -> String {
            iter::once(r"\f.\x.")
                .chain(iter::repeat("(f ").take(n))
                .chain(iter::once("x"))
                .chain(iter::repeat(")").take(n))
                .collect()
        }

        for i in 0..NAT_LIMIT {
            let prog = create_nat(i);
            let expr = Expr::church_encode(i);
            assert_prog!(&prog, expr, "n: {}", i);
        }
    }

    #[test]
    fn test_nat_complex() {
        fn create_nat(n: usize) -> String {
            iter::once(r"\0.\succ.(")
                .chain(iter::repeat("(succ ").take(n))
                .chain(iter::once("0"))
                .chain(iter::repeat(")").take(n))
                .chain([")(", ZERO, ")(", SUCC, ")"])
                .collect()
        }

        for i in 0..NAT_LIMIT {
            let prog = create_nat(i);
            let expr = Expr::church_encode(i);
            assert_prog!(&prog, expr, "n: {}", i);
        }
    }

    #[test]
    fn test_nat_assign() {
        fn create_nat(n: usize) -> String {
            ["zero = ", ZERO, "\n", "succ = ", SUCC, "\n"]
                .into_iter()
                .chain(iter::repeat("(succ ").take(n))
                .chain(iter::once("zero"))
                .chain(iter::repeat(")").take(n))
                .collect()
        }

        for i in 0..NAT_LIMIT {
            let prog = create_nat(i);
            let expr = Expr::church_encode(i);
            assert_prog!(&prog, expr, "n: {}", i);
        }
    }

    #[test]
    fn test_basics() {
        for (prog, expected) in BASICS {
            let expr = parse(expected).unwrap();
            assert_prog!(prog, expr);
        }
    }

    #[test]
    fn test_scoping() {
        for (prog, expected) in SCOPING {
            let expr = parse(expected).unwrap();
            assert_prog!(prog, expr);
        }
    }

    test_example! { test_example_bool, "../examples/bool.lambda" }
    test_example! { test_example_id, "../examples/id.lambda" }
    test_example! { test_example_int, "../examples/int.lambda" }
    test_example! { test_example_list, "../examples/list.lambda" }
    test_example! { test_example_nat, "../examples/nat.lambda" }
    test_example! { test_example_pair, "../examples/pair.lambda" }
    //test_example! { test_example_y, "../examples/y.lambda" }
}
