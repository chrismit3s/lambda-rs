use std::fmt::{self, Display};

pub const LAMBDA: char = '\u{03BB}';
const LINECOMMENT: char = '#';
const NEWLINE: char = '\n';

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum Token<'a> {
    Lambda,
    Ident(&'a str),
    Dot,
    LParen,
    RParen,
    Equals,
    NewLine,
}

impl<'a> Token<'a> {
    pub fn tokenize(s: &'a str) -> Vec<Self> {
        let mut toks = Vec::new();
        let mut ident_start = None;
        let mut in_comment = false;
        for (i, c) in s.char_indices() {
            if c == LINECOMMENT {
                in_comment = true;
            }
            if c == NEWLINE {
                in_comment = false;
            }

            if in_comment {
                continue;
            }

            let next = Self::from(c);
            if let Some(Self::Ident(_)) = next {
                ident_start.get_or_insert(i);
                continue;
            } else if let Some(j) = ident_start {
                toks.push(Self::Ident(&s[j..i]));
                ident_start = None;
            }

            if let Some(Self::NewLine) = next {
                if !matches!(toks.last(), Some(Self::NewLine)) {
                    toks.push(Self::NewLine);
                }
                continue;
            }

            if let Some(tok) = next {
                toks.push(tok);
            }
        }

        if let Some(i) = ident_start {
            toks.push(Self::Ident(&s[i..]));
        } else if let Some(Self::NewLine) = toks.last() {
            toks.pop();
        }

        toks
    }

    pub fn from(c: char) -> Option<Self> {
        match c {
            LAMBDA | '\\' => Some(Self::Lambda),
            '.' => Some(Self::Dot),
            '(' => Some(Self::LParen),
            ')' => Some(Self::RParen),
            '=' => Some(Self::Equals),
            NEWLINE => Some(Self::NewLine),
            c if Token::is_ident(c) => Some(Self::Ident("")),
            _ => None,
        }
    }

    pub fn is_ident(c: char) -> bool {
        c == '_' || c.is_ascii_alphanumeric()
    }
}

impl<'a> Display for Token<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Lambda => write!(f, "'{}'", LAMBDA),
            Self::Ident(ident) => write!(f, "'{}'", ident),
            Self::Dot => write!(f, "'.'"),
            Self::LParen => write!(f, "'('"),
            Self::RParen => write!(f, "')'"),
            Self::Equals => write!(f, "'='"),
            Self::NewLine => write!(f, r"'\n'"),
        }
    }
}
